function AddNewToBuffer(ReqLPN)
% 该函数主要负责将缺失的数据页加载到Buffer中，同时完成读写miss的统计及物理读请求统计
%   和外部数据交换的全局变量
    global Stat;
    global CLRU;
    global DLRU;
    global NandPageArr;
    global CacheMaxAgeIndex;
    global BlkTableArr;
% 配置相关数据对齐的操作
    global SECT_NUM_PER_PAGE;
    global PAGE_NUM_PER_BLK;
    global SECT_NUM_PER_BLK;  
% 和NandPage.Cache_Stat有关的宏定义（在不在缓冲区,在哪个队列）
    global Cache_InCLRU;
    global Cache_InDLRU;
%   --------------------------------------
    if ReqLPN.type~=0
%       统计信息的更新
        Stat.read_miss_count=Stat.read_miss_count+1;
        Stat.physical_read_count=Stat.physical_read_count+1;
%       数据加载操作
        free_pos=FindFreePos(CLRU.list);
        CLRU.list(free_pos)=ReqLPN.start;
%       针对对一次寻找最大的CacheMaxAgeIndex=0，需要处理
        if CacheMaxAgeIndex==0
            NandPageArr(ReqLPN.start).Cache_Age=1;
        else
             NandPageArr(ReqLPN.start).Cache_Age=NandPageArr(CacheMaxAgeIndex).Cache_Age+1;
        end
        CacheMaxAgeIndex=ReqLPN.start;
        NandPageArr(ReqLPN.start).Cache_Stat=Cache_InCLRU;
        NandPageArr(ReqLPN.start).Cache_Update=0;
%         CLRU.size=CLRU.size+1;
        CLRU.size=sum(CLRU.list>0);
%       更新对应的块索引标记(加入新的数据索引)
        tempBlkNum=fix(ReqLPN.start/PAGE_NUM_PER_BLK)+1;
        BlkTableArr(tempBlkNum).TotalSize=BlkTableArr(tempBlkNum).TotalSize+1;
        BlkTableArr(tempBlkNum).CleanNum=BlkTableArr(tempBlkNum).CleanNum+1;
%       存放的应该是该页在CLRU.list的索引位置free_pos
        BlkTableArr(tempBlkNum).Clist=[BlkTableArr(tempBlkNum).Clist,free_pos];
%-----------------------------做一个错误检测----------------------------------------------
        Clist_length=length(BlkTableArr(tempBlkNum).Clist);
        Dlist_length=length(BlkTableArr(tempBlkNum).Dlist);
        if(BlkTableArr(tempBlkNum).TotalSize~=Clist_length+Dlist_length)
            fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum, Dlist_length);
            fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
            fprintf('BlkTable(%d) -TotalSize is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).TotalSize);
            error('the add Index into BlkTableArr exist error!');
        end
        if (BlkTableArr(tempBlkNum).CleanNum~=Clist_length)
            fprintf('BlkTable(%d) -CleanNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).CleanNum);
            fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
            error('the add Index into BlkTableArr exist error!');
        end
%-------------------------------错误检测结束------------------------------------------------
    else
%       统计信息的更新 
        Stat.write_miss_count=Stat.write_miss_count+1;
        Stat.physical_read_count=Stat.physical_read_count+1;
%       数据加载操作       
        free_pos=FindFreePos(DLRU.list);
        DLRU.list(free_pos)=ReqLPN.start;
%       针对对一次寻找最大的CacheMaxAgeIndex=0，需要处理
        if CacheMaxAgeIndex==0
            NandPageArr(ReqLPN.start).Cache_Age=1;
        else
             NandPageArr(ReqLPN.start).Cache_Age=NandPageArr(CacheMaxAgeIndex).Cache_Age+1;
        end
        CacheMaxAgeIndex=ReqLPN.start;
        NandPageArr(ReqLPN.start).Cache_Stat=Cache_InDLRU;
        NandPageArr(ReqLPN.start).Cache_Update=1;
%         DLRU.size=DLRU.size+1;
        DLRU.size=sum(DLRU.list>0);
%       更新对应的块索引标记(加入新的数据索引)
        tempBlkNum=fix(ReqLPN.start/PAGE_NUM_PER_BLK)+1;
        BlkTableArr(tempBlkNum).TotalSize=BlkTableArr(tempBlkNum).TotalSize+1;
        BlkTableArr(tempBlkNum).DirtyNum=BlkTableArr(tempBlkNum).DirtyNum+1;
%       存放的应该是该页在DLRU.list的索引位置free_pos
        BlkTableArr(tempBlkNum).Dlist=[BlkTableArr(tempBlkNum).Dlist,free_pos];
%-----------------------------做一个错误检测----------------------------------------------
        Clist_length=length(BlkTableArr(tempBlkNum).Clist);
        Dlist_length=length(BlkTableArr(tempBlkNum).Dlist);
        if(BlkTableArr(tempBlkNum).TotalSize~=Clist_length+Dlist_length)
            fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum, Dlist_length);
            fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
            fprintf('BlkTable(%d) -TotalSize is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).TotalSize);
            error('the add Index into BlkTableArr exist error!');
        end
        if (BlkTableArr(tempBlkNum).CleanNum~=Clist_length)
            fprintf('BlkTable(%d) -DirtyNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).DirtyNum);
            fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum,Dlist_length);
            error('the add Index into BlkTableArr exist error!');
        end
%-------------------------------错误检测结束------------------------------------------------       
    end
%  ------------------------------DEBUG-------------------------------------------
%              加入代码调试判断CLRU和DLRU的实际大小时候符合预期
           if (sum(CLRU.list>0)~=CLRU.size||sum(DLRU.list>0)~=DLRU.size)
               fprintf('the CLRU.size is %d\n',CLRU.size);
               fprintf('the CLRU.list nozeros entry num is %d\n',sum(CLRU.list>0)); 
               fprintf('the DLRU.size is %d\n',DLRU.size);
               fprintf('the DLRU.list noezeors entry num is %d\n',sum(DLRU.list>0));
               error('find error in AddNewToBuffer');
           end
%   --------------------------DEBUG-----------------------------------------
    
end