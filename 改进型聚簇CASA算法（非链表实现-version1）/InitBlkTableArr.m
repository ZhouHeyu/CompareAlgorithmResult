function BlkTableArr=InitBlkTableArr(Max_size)
%    该函数完成对BlkTable块索引集合的初始化，其中逻辑块号从1开始计数，需要注意的是
%    BlkTable是个结构体数组，下标即逻辑块号，其中这个BlkTable包含的缓冲区脏页的个数，干净页的个数
%    记录的同属于该块脏页，在DLRU.list中的位置索引；同于该块干净页，在CLRU.list中的位置索引
    global PAGE_NUM_PER_BLK;
    BlkTable.TotalSize=0;
    BlkTable.CleanNum=0;
    BlkTable.DirtyNum=0;
%    记录的同属于该块脏页，在CLRU.list中的位置索引
    BlkTable.Clist=[];
%    以空间换时间，直接定义全部的块大小的页，避免循环遍历，reqLPN%PAGE_NUM_PER_BLK的偏移量就能计算该页的位置。。。
    
%    记录的同属于该块脏页，在DLRU.list中的位置索引
    BlkTable.Dlist=[];
    BlkTableArr=repmat(BlkTable,[1,Max_size]);
    return;
end