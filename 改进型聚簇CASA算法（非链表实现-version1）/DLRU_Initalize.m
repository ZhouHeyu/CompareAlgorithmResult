function DLRU_Initalize()
% DLRU是一个全局变量在，主函数main.m中也需要定义global DLRU
global DLRU;
global buf_size;
% list存放的是访问的LPN号，这里的LPN号都是在原来的基础上加1偏移
DLRU.list=zeros(1,buf_size);
% 这里的size大小是指list中包含非0的项
DLRU.size=0;
end