function [MinIndex,BoundValue]=FindMinAndBound(Arr,threshold)
%     该函数找到数组Arr(也就是DLRU.list(index)--->NandPage.Cache_age最小)
%    输入参数的threshold是数组中有效项前百分比的项，BoundValue也就是当前百分比最小的Cache_age的值
    global NandPageArr;
    MinAge=9999999999;
%   NL是数组中的非零项也就是其中的有效数据项
    NL=sum(Arr>0);
    MinIndex=0;
    BoundValue=0;
%   如果不存在有效的数据项则直接返回,返回数据项都是无效的数据0，回调函数注意判断
    if NL==0
        return;
    end
    L=length(Arr);
%   注意取整
    Bound=fix(NL*threshold);
%    tempArr存放之后读取对比的Cache_age
    tempArr=zeros(1,NL);
    for i=1:L
%       当非零项遍历完,没有必要再遍历了(节省不必要的遍历)
        if(NL<=0)
            break;
        end
%       确保数组中该位置存放的是有效的LPN
        if Arr(i)>0
    %       找到最小的age则替换
            if(NandPageArr(Arr(i)).Cache_Age<MinAge)
                MinAge=NandPageArr(Arr(i)).Cache_Age;
                MinIndex=i;
            end
%           存储这些Cache_Age以便之后寻找BoundValue
            tempArr(NL)=NandPageArr(Arr(i)).Cache_Age;
            NL=NL-1;
        end
    end
%   针对tempArr的数据进行重新的排序(降序，冷的数据项在后面)，找到BoundValue
    tempArr=sort(tempArr,'descend');
    BoundValue=tempArr(Bound);
end