function [LRU]=FindDrityVictimInLRU(LRU)
%    该函数是寻找该LRU队列中的脏数据项进行剔除（在该LRU.FC为0时才启用该函数），基于二次机会剔除
%     函数返回的时，队列的LRU位置的二次机会的flag标志为0（即LRU.list的第三行的标志位为0）
%     debug test
     [L,C]=size(LRU.list);
    if LRU.length~=C
%   代码复用，错误判断可能出现两种更新错误，在主函数需要判断
           disp ('Error hanppend in Find Dirty Vicitim!');
           error('LRU_length Update Maybe Exist Error!\n');
    end
%    handle for LRU is empty
    if LRU.length==0
        return ;
    end
%  基于二次机会策略选择剔除项
    while (1)
%      调用该函数的时候不应该出现队列中有干净页出现的情况，如果出现了说明主函数中处理存在逻辑错误
       if LRU.list(2,end)==0
           disp('Call FindDrityVictimInLRU When LRU does not have clean page!!');
           error('UpdateLRUFC may has error!');
       else
%       找到访问位为０的候选项，在函数调用结束的时候作为剔除对象
            if LRU.list(3,end)==0
                break;
            else
                LRU.list(3,end)=0;
                LRU.list=[LRU.list(:,end),LRU.list(:,1:end-1)];
            end
       end
    end%end-of-while
    
end%end-of-function