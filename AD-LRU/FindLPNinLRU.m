function [Index]=FindLPNinLRU(LPN,LRU)
% 该函数用来遍历输入的LRU.list中是否存在对应的LPN号,如果不存在则返回的Index=0
    Index=0;
%   debug test
     [L,C]=size(LRU.list);
    if LRU.length~=C
%         代码复用，错误判断可能出现两种更新错误，在主函数需要判断
           error('LRU_length Update May Exist Error!\n');
    end
    %    handle for LRU is empty
    if LRU.length==0
        return ;
    end
%    遍历寻找该LRU中是否存在对应的LPN号
    for Pindex=1:LRU.length
        if LRU.list(1,Pindex)==LPN
            Index=Pindex;
            break;
        end
    end
    return ;
end