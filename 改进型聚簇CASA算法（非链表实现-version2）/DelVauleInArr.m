function Arr=DelVauleInArr(Arr,Value)
% 该函数首先完成对输入的Value的值匹配，找到指定的位置，之后删除该数组中元素Value
% 注意函数回调时，更新对应数组Arr的大小计算,这里操纵的Arr是包含全部有效值的数组，大小可变不是固定的
    Index=FindValueInArr(Arr,Value);
%   Arr删除指定的值
% ---------做个错误判断---------------------------
    L=length(Arr);
    if Index>L || L<=0
        fprintf('Error happened In DelValueInArr\n');
        fprintf('Arr-Size is %d\t Del-Index is %d\n',L,Index);
        error('please find release debug\n');
    end
%------------------------------------------------
    Arr=[Arr(1:Index-1),Arr(Index+1:end)];
end