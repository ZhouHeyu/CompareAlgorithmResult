function ResetNandPage(LPN)
% 该函数主要完成对指定的LPN号的Page相关的标识位重新置为初始值
    global NandPageArr;
    % 和NandPage.Cache_Stat有关的宏定义（在不在缓冲区,在哪个队列）
    global Cache_Invalid;
    global Cache_InCLRU;
    global Cache_InDLRU;
    NandPageArr(LPN).Cache_Stat= Cache_Invalid;
    NandPageArr(LPN).Cache_Age=0;
    NandPageArr(LPN).Cache_Update=0;
end