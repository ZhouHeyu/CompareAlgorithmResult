function NandPageArr=InitNandPage(Max_size)
% 该函数主要完成对总的闪存数据页的初始化，Max_Size表示当前最大的闪存容量(以数据页为单位)
% NandPage是个结构体，包含(Cache_Stat)是否在缓冲区,(Cache_Age)在缓冲区的滞留时间,（Cache_Update）在缓冲区是否被修改
    NandPage.Cache_Age=0;
    NandPage.Cache_Stat=0;
    NandPage.Cache_Update=0;
    NandPageArr=repmat(NandPage,[1,Max_size]);
    return;
% 这里做个说明,NandPageArr的下标就是我们理解的LPN号，因为Matlab的数据从1开始，但是LPN从0开始所以做个偏移，
% 故Max_size是实际最大地址页加1
end