function UpdateHistoryStat(index,CurrStat,tau,CL,DL)
global history;
history.tau(index)=tau;
history.ReadHitCount(index)=CurrStat.ReadHitCount;
history.ReadMissCount(index)=CurrStat.ReadMissCount;
history.WriteHitCount(index)=CurrStat.WriteHitCount;
history.WriteMissCount(index)=CurrStat.WriteMissCount;
history.TotalCount(index)=CurrStat.TotalCount;
history.AveDelay(index)=CurrStat.AveDelay;
history.CLRU_length(index)=CL;
history.DLRU_length(index)=DL;
end